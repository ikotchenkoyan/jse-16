package ru.t1.kotchenko.tm.command.user;

import ru.t1.kotchenko.tm.util.TerminalUtil;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "user login";
    }

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }
}
