package ru.t1.kotchenko.tm.command.project;

import ru.t1.kotchenko.tm.model.Project;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        getProjectTaskService().removeProjectById(project.getId());
    }

    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

    @Override
    public String getName() {
        return "project-remove-by-index";
    }

}
