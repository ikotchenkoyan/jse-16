package ru.t1.kotchenko.tm.command.task;

import ru.t1.kotchenko.tm.model.Task;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(id);
        showTask(task);
    }

    @Override
    public String getDescription() {
        return "Show task by index.";
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

}
