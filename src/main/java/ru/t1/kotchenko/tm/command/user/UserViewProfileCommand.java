package ru.t1.kotchenko.tm.command.user;

import ru.t1.kotchenko.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "view profile of current user";
    }

    @Override
    public String getName() {
        return "view-user-profile";
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }
}
