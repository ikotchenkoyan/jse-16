package ru.t1.kotchenko.tm.command.user;

public class UserLoginCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "user logout";
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }
}
