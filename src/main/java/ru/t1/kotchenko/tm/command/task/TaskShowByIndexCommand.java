package ru.t1.kotchenko.tm.command.task;

import ru.t1.kotchenko.tm.model.Task;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().findOneByIndex(index);
        showTask(task);
    }

    @Override
    public String getDescription() {
        return "Change task status by index.";
    }

    @Override
    public String getName() {
        return "task-show-by-index";
    }

}
