package ru.t1.kotchenko.tm.command.project;

import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusName = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusName);
        getProjectService().changeProjectStatusById(id, status);
    }

    @Override
    public String getDescription() {
        return "Change project status by id.";
    }

    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

}
