package ru.t1.kotchenko.tm.command.user;

import ru.t1.kotchenko.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "change password of current user";
    }

    @Override
    public String getName() {
        return "change-user-password";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER CHANGE PROFILE]");
        System.out.println("[ENTER NEW PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }
}
