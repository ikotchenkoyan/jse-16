package ru.t1.kotchenko.tm.command.task;

import ru.t1.kotchenko.tm.util.TerminalUtil;

public class TaskUnbindToProjectCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskToProject(projectId, taskId);
    }

    @Override
    public String getDescription() {
        return "Unbind task to project.";
    }

    @Override
    public String getName() {
        return "task-unbind-to-project";
    }

}
