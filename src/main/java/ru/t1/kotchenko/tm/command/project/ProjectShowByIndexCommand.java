package ru.t1.kotchenko.tm.command.project;

import ru.t1.kotchenko.tm.model.Project;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        showProject(project);
    }

    @Override
    public String getDescription() {
        return "Change project status by index.";
    }

    @Override
    public String getName() {
        return "project-show-by-index";
    }

}
