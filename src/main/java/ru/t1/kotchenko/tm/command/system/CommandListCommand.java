package ru.t1.kotchenko.tm.command.system;

import ru.t1.kotchenko.tm.command.AbstractCommand;

public class CommandListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        for (AbstractCommand command : getCommandService().getTerminalCommands()) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getArgument() {
        return "-c";
    }

    @Override
    public String getDescription() {
        return "Show application commands.";
    }

    @Override
    public String getName() {
        return "commands";
    }

}
