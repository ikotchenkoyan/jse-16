package ru.t1.kotchenko.tm.command.system;

import ru.t1.kotchenko.tm.command.AbstractCommand;

public class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        for (AbstractCommand command : getCommandService().getTerminalCommands()) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show application arguments.";
    }

    @Override
    public String getName() {
        return "arguments";
    }

}
