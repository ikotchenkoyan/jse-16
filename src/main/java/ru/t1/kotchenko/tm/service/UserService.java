package ru.t1.kotchenko.tm.service;

import ru.t1.kotchenko.tm.api.repository.IUserRepository;
import ru.t1.kotchenko.tm.api.service.IUserService;
import ru.t1.kotchenko.tm.enumerated.Role;
import ru.t1.kotchenko.tm.exception.entity.UserNotFoundException;
import ru.t1.kotchenko.tm.exception.field.*;
import ru.t1.kotchenko.tm.exception.user.ExistEmailException;
import ru.t1.kotchenko.tm.exception.user.ExistLoginException;
import ru.t1.kotchenko.tm.model.User;
import ru.t1.kotchenko.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return userRepository.add(user);
    }

    @Override
    public User create(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new ExistEmailException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User add(User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findOneByLogin(String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findOneByEmail(String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = userRepository.findOneByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User remove(User user) {
        if (user == null) return null;
        return userRepository.remove(user);
    }

    @Override
    public User removeById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findOneById(id);
        return remove(user);
    }

    @Override
    public User removeOneByLogin(String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findOneByLogin(login);
        return remove(user);
    }

    @Override
    public User removeOneByEmail(String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = userRepository.findOneByEmail(email);
        return remove(user);
    }

    @Override
    public User setPassword(String id, String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(String id, String firstName, String lastName, String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isEmailExist(login);
    }

    @Override
    public Boolean isEmailExist(String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExist(email);
    }
}
