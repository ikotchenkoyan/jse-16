package ru.t1.kotchenko.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Argument not supported.");
    }

    public CommandNotSupportedException(final String argument) {
        super("Error! Argument ''" + argument + "'' not supported.");
    }

}
